# Copula-based conformal bounding box prediction
## Abstract
Object detection is an important vision task, and providing statistical guarantees around such detections can be of critical importance. So far, most conformal bounding box regression approaches do not simultaneously account for heteroscedasticity and dependencies between the residuals of each dimension. In this paper, we examine the importance of such dependencies and heteroscedasticity in the context of multi-target conformal regression, we apply copula-based conformal prediction methods to model them and to improve the volume of bounding box prediction regions. We compare these methods to the state-of-the-art conformal object detection approaches (on the KITTI \& the BDD100K autonomous driving benchmarks) and the empirical copula-based method shows high-efficiency results that are robust w.r.t. heteroscedasticity and also robust w.r.t. the structure of the dependencies.

The full report:

- [Copula-based conformal prediction for object detection: a more efficient approach](https://proceedings.mlr.press/v230/mukama24a.html)

<!-- 🚩  - Checkout demo.ipynb for more details about the results ⚠️ -->

## Examples
Inferring bounding box prediction regions with **95%** coverage/validity rate, on **BDD100K**, with the **Empirical DE-CCP** approach. The ground truth boxes are shown in green and their prediction regions are highlighted in white (with blue borders).
![Visual demo](demo.gif)

## Requirements
- Conda v23.11.0 
- CUDA v12.0+ (drv N° 525.147.05+)
- All the packages in environment.yml

## Tutorial
- run setup.sh
- fill experiments.yaml
- run experiments.py
- run demo.ipynb
