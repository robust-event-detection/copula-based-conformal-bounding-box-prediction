import sys
import yaml
import torch
import pickle
import warnings

from pathlib import Path
from shutil import rmtree
from typing import Tuple, List, Any

from src.detectors import Detector, YOLOv8
from src.reporting import UQEvalResults
from src.utils import init_random_state
from src.data import (
    SyntheticResiduals,
    FilteredKitti, 
    FilteredBDD100K, 
    Split, split_dataset
)
from src.conformalizers import (
    Conformalizer,
    BonferroniAdditiveConformalizer,
    MaxAdditiveConformalizer,
    ImprovedIndependentConformalizer,
    ImprovedGumbelConformalizer,
    ImprovedEmpiricalConformalizer,
)


def setup_experiment(exp_configs: dict) -> Tuple:
    UQers, datasets = [], []

    if exp_configs['UQ algos']['Bonferroni']:
        UQers.append(BonferroniAdditiveConformalizer())
    if exp_configs['UQ algos']['Max additive']:
        UQers.append(MaxAdditiveConformalizer())
    if exp_configs['UQ algos']['Independent DE-CCP']:
        UQers.append(ImprovedIndependentConformalizer())
    if exp_configs['UQ algos']['Gumbel DE-CCP']:
        UQers.append(ImprovedGumbelConformalizer())
    if exp_configs['UQ algos']['Empirical DE-CCP']:
        UQers.append(ImprovedEmpiricalConformalizer())

    if exp_configs['Datasets']['Correlated synthetic residuals']:
        datasets.append(SyntheticResiduals())
    if exp_configs['Datasets']['Uncorrelated synthetic residuals']:
        datasets.append(SyntheticResiduals(cov_matrix=None))
    if exp_configs['Datasets']['real KITTI']:
        datasets.append(FilteredKitti('datasets', download=True))
    if exp_configs['Datasets']['real BDD100K']:
        datasets.append(FilteredBDD100K('datasets', download=True))
    
    parameters = [
        {'epsilon': eps,
         'k_folds': exp_configs['UQ algos']['calibration']['k_folds'],
         'cal_ratio': exp_configs['UQ algos']['calibration']['cal_ratio'],
         'iou_threshold': exp_configs['UQ algos'][
             'calibration']['pairing_iou_th'],
         'confidence_threshold': exp_configs['UQ algos'][
             'calibration']['pairing_conf_th'],
         'epochs': exp_configs['OD algos']['training']['epochs'],
         'val_size': exp_configs['OD algos']['training'][
             'validation sample size'],
         'from_scratch': exp_configs['OD algos']['training']['from_scratch'],
         'device': torch.device(exp_configs['OD algos']['training']['device'])
         } for eps in exp_configs['UQ algos']['calibration']['epsilons']
    ]
    return UQers, parameters, datasets


def load_checkpoint(res: UQEvalResults, dtst: Any, parameters: List) -> Tuple:
    saved_res = list(Path(res.dest_folder, "temp").glob("res_*.pkl"))
    saved_splits = list(Path(res.dest_folder, "temp").glob("splits_*.pkl"))

    saved = len(saved_res) >= 1 and len(saved_splits) >= 1
    if not saved:
        splits = split_dataset(dtst, **parameters[0])
    
    else:
        with open(sorted(saved_res)[-1], 'rb') as f:
            res = pickle.load(f)
        with open(sorted(saved_splits)[-1], 'rb') as f:
            splits = pickle.load(f)
    return splits, res


def run_experiment(UQers: List[Conformalizer], parameters: List,
                   split: Split, detector: Detector,
                   res: UQEvalResults, nn_perfs: str) -> UQEvalResults:
    for conformalizer in UQers:
        for params in parameters:
            conformalizer.set_parameters(**params)
            cal_res = conformalizer.calibrate(split.cal_pairs)
            test_res = conformalizer.conformalize(split.test_pairs)
            res.add_results(
                split, conformalizer, detector,
                params, nn_perfs, cal_res, test_res,
                conformalizer.dissimilarities)


def save_checkpoint(split: Split, splits: List, res: UQEvalResults) -> Tuple:
    temp_folder = Path(res.dest_folder, "temp")
    temp_folder.mkdir(exist_ok=True)

    res_fname = f"res_after_N°{split.label:03d}.pkl"
    with open(f"{temp_folder}/{res_fname}", 'wb') as f:
        pickle.dump(res, f)

    splits_fname = f"splits_after_N°{split.label:03d}.pkl"
    with open(f"{temp_folder}/{splits_fname}", 'wb') as f:
        split.finish()
        pickle.dump(splits, f)


def remove_checkpoint(res: UQEvalResults) -> None:
    rmtree(Path(res.dest_folder, "temp")) #, ignore_errors=True) ??


def experiment_with_detector(detector: Detector, res: UQEvalResults,
                             exp_configs: dict, res_label: str) -> None:
    UQers, parameters, datasets = setup_experiment(exp_configs)
    splits = []
    for dataset in datasets:
        res_label_ = res_label.replace('dts', dataset.name)
        if Path(res.dest_folder, res_label_ + ".pkl").is_file():
            continue
        splits, res = load_checkpoint(res, dataset, parameters)

        for split in splits:
            if not split.is_finished:
                if split.dataset.is_synthetic():
                    nn_perfs, det = None, Detector()
                else:
                    nn_perfs = detector.train(split, **parameters[0])
                    det = detector
                split.gen_data_pairs(det)
                run_experiment(UQers, parameters, split, det, res, nn_perfs)
                save_checkpoint(split, splits, res)
        res.save(label=res_label_, time=False, reinit=True)
        remove_checkpoint(res)
        

if __name__ == "__main__":
    if not sys.warnoptions:
        warnings.simplefilter("ignore")
    init_random_state()

    with open("experiments.yaml", "r") as f:
        cfg = yaml.load(f, Loader=yaml.FullLoader)

    detectors = []
    if cfg["OD algos"]["YOLOv8 D."]:
        detectors.append(YOLOv8())

    res = UQEvalResults(dest_folder=cfg['logs']['folder'])
    res_label = f"{cfg['logs']['prefix']}_dts_{cfg['logs']['suffix']}"
    for detector in detectors:
        experiment_with_detector(detector, res, cfg, res_label)
    print(">> Ending experiments ...")