import pickle
import numpy as np
import pandas as pd

from pathlib import Path
from pandas import DataFrame
from datetime import datetime
from typing import List, Tuple
from matplotlib import pyplot as plt

from src.data import Split
from src.detectors import Detector
from src.conformalizers import Conformalizer
from src.metrics import coverage, efficiency


class UQEvalResults():
    def __init__(self, dest_folder: str = "results"):
        self.logs = dict()
        assert Path(dest_folder).is_dir()
        self.dest_folder = dest_folder

    def add_results(self, split: Split, conformalizer: Conformalizer, 
                    detector: Detector, params: List, tra_res: DataFrame,
                    cal_res: DataFrame, test_res: List, 
                    dissimilarities: DataFrame) -> None:
        epsilon = params["epsilon"]
        new_key = (
            detector.name, split.dataset.name, 
            split.label, conformalizer.name, epsilon
        )
        new_entry = {'params': params, 'tra_res': tra_res, 
                     'cal_res': cal_res, 'test_res': test_res, 
                     'conformalizer': conformalizer, 
                     'cal_dissims': dissimilarities}
        msg = "detector: %s, dataset: %s, fold: %s, method: %s, epsilon: %s"
        print(">> Logging results for", msg % new_key)
        self.logs[new_key] = new_entry

    def save(self, label: str, time: bool, 
             dest_folder: str = None, reinit: bool = False) -> None:
        dest_folder = self.dest_folder if dest_folder is None else dest_folder
        now = f"_{datetime.now():%d-%m-%Y_%H:%M:%S}" if time else ''
        filename = f"{label}{now}.pkl"

        with open(f"{dest_folder}/{filename}", 'wb') as f:
            pickle.dump(self.logs, f)
        self.logs  = dict() if reinit else self.logs

    def load(self, filename: str):
        with open(filename, 'rb') as f:
            loaded = pickle.load(f)
        self.logs = loaded


class GifReporter():
    def __init__(self, num_frames: int, dest: str) -> None:
        pass

    def report(self, res: UQEvalResults, save=False) -> List:  # Gif
        pass


class SummaryReporter():
    def __init__(self, epsilons: List, approaches: List, detectors: List,
                 #figsize: Tuple[int] = (15, 5),
                 summarize_validity: bool = True,
                 markers:List=['v', 'D', '$0$', '+', 'x'],
                 k_folds: int = 10, mode: str = 'test') -> None:
        self.approaches = approaches
        self.markers = markers
        self.detectors = detectors
        self.k_folds = k_folds
        self.epsilons = epsilons
        self.mode = mode
        self.fig, axes = plt.subplots(1, 1, figsize=(5, 5)) 
        self.axes = [axes] #plt.subplots(1, 3, figsize=figsize)
        self.summarize_validity = summarize_validity
        if self.summarize_validity:
            self.wspace = None
            self.metric = coverage
            self.title = 'Calibration curves'
            self.x_lim = self.y_lim = (0, 1)
            self.x_label = r'$1 - \epsilon^g$'
            self.y_label = "Empirical coverage"
        else:
            self.wspace = 0.3
            self.metric = efficiency
            self.title = 'Efficiency curves'
            self.x_lim, self.y_lim = (0, 1), (None, None)
            self.x_label = r'$1 - \epsilon^g$'
            self.y_label = "Average volume"

    def report(self, results: UQEvalResults, dataset: str) -> pd.DataFrame:
        reported = 'validity' if self.summarize_validity else 'efficiency'
        print(f">> Reporting {reported} ...")
        summary_table = self.tabulate_summary(results, dataset)
        self.plot_summary(results, summary_table)
        return summary_table

    def tabulate_summary(self, results: UQEvalResults, 
                         dataset: str) -> pd.DataFrame:
        pds = []
        for detector in self.detectors:
            df = {self.x_label: [], }

            for i, approach in enumerate(self.approaches):
                df[approach] = []
                for epsilon in self.epsilons:
                    metrics = []
                    df[approach]
                    df[self.x_label].append(1 - epsilon) if i == 0 else None

                    for fold in range(self.k_folds):
                        logs_key=(detector, dataset, fold, approach, epsilon)
                        logs = results.logs[logs_key][f"{self.mode}_res"]
                        metrics.append(self.metric(logs))

                    mean, std = np.mean(metrics), np.std(metrics)
                    df[approach].append(self.format_cell([mean, std]))
            pds.append(pd.DataFrame(data=df))
        return pds

    def format_cell(self, x: List) -> str:
        if self.summarize_validity:
            cell_txt = f"{x[0]:.3f}\u00B1{x[1]:.3f}"
        else:
            cell_txt = f"{x[0]:.2e}\u00B1{x[1]:.2e}"
        return cell_txt

    def plot_summary(self, results: UQEvalResults, tables: List) -> None:
        for i, tb in enumerate(tables):
            self.scatter(self.axes[i], tab=tb)
            self.axes[i].grid()
        self.format_plots(results, len(tables))
        plt.show()

    def scatter(self, ax: plt.Axes, tab: pd.DataFrame) -> None:
        get_mean = lambda x: x.split('\u00B1')[0]
        for mrk, app in zip(self.markers, self.approaches):
            x = tab[r'$1 - \epsilon^g$'].astype(float)
            y = tab[app].apply(get_mean).astype(float)
            ax.scatter(x.values, y.values, label=app, marker=mrk)

    def format_plots(self, res: UQEvalResults, n_plots: int) -> None:
        self.fig.suptitle(self.title, size='xx-large', weight='bold')
        [((self.plot_calibration_line(self.axes[i], res)
            if self.summarize_validity else None),
          self.axes[i].legend(), 
          self.axes[i].set_xlim(*self.x_lim),
          self.axes[i].set_ylim(*self.y_lim),
          self.axes[i].set_ylabel(self.y_label),
          self.axes[i].set_xlabel(self.x_label)) for i in range(n_plots)]
        plt.subplots_adjust(wspace=self.wspace)

    def plot_calibration_line(self, ax: plt.Axes, res: UQEvalResults) -> None:
        cvgs = sorted(set([(1 - epsilon) for epsilon in self.epsilons]))
        ax.plot(cvgs, cvgs, 'k--', label='Calibration line')
