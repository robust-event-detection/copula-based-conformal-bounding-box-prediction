import torch
import random
import numpy as np
import pandas as pd
from typing import List
from torchvision.ops import box_iou
from ultralytics.engine.results import Results
from scipy.optimize import linear_sum_assignment


SEED = 12345


def init_random_state(seed: int = SEED) -> None:
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)


def get_yolo_bboxes(rslts: Results, conf_th: float) -> List[dict]:
    objects = []
    names = rslts.names
    for bb in rslts.boxes:
        cls_ = names[int(bb.cls.cpu().item())]
        bbox = bb.xyxy.squeeze().tolist()
        if bb.conf >= conf_th:  # By default, Yolo v8's conf_th = 0.25
            objects.append({'type': cls_, 'bbox': bbox})
    return objects


def get_assignements_costs(gts: List, preds: List) -> np.ndarray:
    gts_t = torch.tensor([gt['bbox'] for gt in gts])
    preds_t = torch.tensor([p['bbox'] for p in preds])
    cost_matrix = 1 - box_iou(gts_t, preds_t)
    return cost_matrix


def hmatch_preds2gt(preds: List[dict], gts: List[dict], 
                    iou_th: float = 0.3, 
                    cost_matrix: np.ndarray=None) -> List[List]:
    matchings, matched_gts, matched_preds = [], [], []
    if cost_matrix is None and gts != [] and preds != []:
        cost_matrix = get_assignements_costs(gts, preds).cpu()

    if cost_matrix is not None:
        gt_indices, preds_indices = linear_sum_assignment(cost_matrix)
        for i, j in zip(gt_indices, preds_indices):
            if 1 - cost_matrix[i, j] >= iou_th:
                matchings.append((gts[i]['bbox'], preds[j]['bbox']))

    if matchings != []:
        matched_gts, matched_preds = list(zip(*matchings)) 
    else:
        matched_gts, matched_preds = None, None
    return matched_gts, matched_preds


def make_empty_entry(UQers: List = ["Max additive", "Empirical DE-CCP"],
                     Bounds: List = ["Lower bound", "Upper bound"],
                     obj_fields: List=["x_min", "y_min", "x_max", "y_max"],
                     imgs: List = [f"{-1:06d}.png"],
                     objs: List = [0]) -> pd.DataFrame:
    rows = pd.MultiIndex.from_product(
        (imgs, objs), names=["images", "objects"])
    columns = pd.MultiIndex.from_product(
        (UQers, obj_fields, Bounds),
        names=["UQ methods", "object's fields", "Bounds"])
    empty_df = pd.DataFrame(index=rows, columns=columns)
    return empty_df
