from pandas import DataFrame


def abs_diff(pairs: DataFrame) -> DataFrame:
    diffs = pairs["Ground truths"] - pairs["Predictions"]
    abs_diffs = diffs.abs()
    return abs_diffs


def coverage(results: DataFrame) -> float:
    method = results.columns[-1][0]
    gt_boxes = results["Ground truths"]
    gt_boxes = gt_boxes.xs("Exact value", level="Bounds", axis=1)
    outer_boxes = results[method].xs("Upper bound", level="Bounds", axis=1)
    inner_boxes = results[method].xs("Lower bound", level="Bounds", axis=1)
    validities = (  
        (gt_boxes["x_min"] >= outer_boxes["x_min"]) 
        & (gt_boxes["y_min"] >= outer_boxes["y_min"])
        & (gt_boxes["x_max"] <= outer_boxes["x_max"]) 
        & (gt_boxes["y_max"] <= outer_boxes["y_max"])
        & (gt_boxes["x_min"] <= inner_boxes["x_min"])
        & (gt_boxes["y_min"] <= inner_boxes["y_min"])
        & (gt_boxes["x_max"] >= inner_boxes["x_max"]) 
        & (gt_boxes["y_max"] >= inner_boxes["y_max"])
    )
    cvg = validities.value_counts().get(True, 0) / len(validities)
    return cvg


def efficiency(results: DataFrame) -> float:
    method = results.columns[-1][0]
    outer_boxes = results[method].xs("Upper bound", level="Bounds", axis=1)
    inner_boxes = results[method].xs("Lower bound", level="Bounds", axis=1)
    volumes = (outer_boxes - inner_boxes).abs().product(axis=1)
    avg_volume = volumes.mean()
    return avg_volume