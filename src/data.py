import csv
import json
import shutil
import hashlib
import requests
import pandas as pd
import numpy as np

from PIL import Image
from pathlib import Path
from tqdm.auto import tqdm
from zipfile import ZipFile
from torchvision.datasets import Kitti
from torch.utils.data import random_split, Dataset, Subset
from typing import Any, Callable, Tuple, List
from ultralytics.engine.results import Results as YOLO_preds
from sklearn.model_selection import KFold

from src.utils import get_yolo_bboxes, hmatch_preds2gt, make_empty_entry, SEED


class Split():
    def __init__(self, dataset: Dataset, subsets: List[Subset],
                 label: int = 1, **kwargs) -> None:
        self.label = label
        self.is_finished = False
        self.dataset = dataset
        self.iou_th = kwargs.get('iou_threshold', 0.3)
        self.conf_th = kwargs.get('confidence_threshold', 0.3)
        self.tra_subset, self.cal_subset, self.test_subset = subsets
        self.cal_pairs, self.test_pairs = None, None

    def gen_data_pairs(self, detector: Callable) -> None:
        print(f">> Pairing bboxes for fold N° {self.label} ...")
        if self.cal_pairs is None:
            self.gen_cal_pairs(detector)
        if self.test_pairs is None:
            self.gen_test_pairs(detector)

    def gen_cal_pairs(self, detector: Callable) -> None:
        self.cal_pairs = self.gen_pairs(self.cal_subset, detector)

    def gen_pairs(self, subset: Subset, det: Callable) -> pd.DataFrame:
        pairs = make_empty_entry(UQers=["Ground truths", "Predictions"],
                                 Bounds=["Exact value"]) 
        for idx in subset.indices:
            item = self.dataset[idx]
            if item[1] != []:
                _, raw_gts, fname = item
                raw_preds = _ if self.dataset.is_synthetic() else det(item)
                gts, preds = self.extract_values(raw_preds, raw_gts)
                if preds is not None:
                    self.append_to_pairs(pairs, fname, "Ground truths", gts)
                    self.append_to_pairs(pairs, fname, "Predictions", preds)
        pairs.drop([(f"{-1:06d}.png", 0)], inplace=True)
        return pairs

    def extract_values(self, pred: Any, gts: Tuple) -> List:
        if isinstance(pred, YOLO_preds):
            pred_vals = get_yolo_bboxes(pred, self.conf_th)
            gt_vals, pred_vals = hmatch_preds2gt(pred_vals, gts, self.iou_th)
        else:
            gt_vals, pred_vals = [gt['bbox'] for gt in gts], pred.tolist()
        return gt_vals, pred_vals

    def append_to_pairs(self, pairs: pd.DataFrame, fname: str,
                        source: str, data: List) -> None:
        for i, datum in enumerate(data):
            if (fname, i) not in pairs.index:
                pairs.loc[(fname, i), :] = pd.Series(dtype='float64')
            pairs.loc[fname, i].at[source] = datum

    def gen_test_pairs(self, detector: Callable) -> None:
        self.test_pairs = self.gen_pairs(self.test_subset, detector)
    
    def finish(self, ) -> None:
        self.is_finished = True


def split_dataset(dataset: Dataset, **kwargs) -> Split:
    k_folds = kwargs.get("k_folds", 10)
    cal_ratio = kwargs.get("cal_ratio", 0.12)
    ratios = [1 - cal_ratio, cal_ratio]
    kf = KFold(n_splits=k_folds, shuffle=True, random_state=SEED)
    splits = []
    for i, (train_set, test_set) in enumerate(kf.split(dataset)):
        train_set = Subset(dataset, train_set)
        test_set = Subset(dataset, test_set)
        train_set, cal_set = random_split(train_set, ratios)
        subsets = (train_set, cal_set, test_set)
        split = Split(dataset, subsets, i, **kwargs)
        splits.append(split)
    return splits


class SyntheticResiduals(Dataset):
    def __init__(self, img_size: Tuple = (640, 640),
                 size: int = 4000, means: List = [0, 0, 0, 0],
                 cov_matrix: List = [[2, 0.8, 0.8, 0.8],
                                     [0.8, 1.5, 0.8, 0.8],
                                     [0.8, 0.8, 15, 0.8],
                                     [0.8, 0.8, 0.8, 1.5]]) -> None:
        super().__init__()
        self.name = "Correlated" if cov_matrix is not None else "Uncorrelated"
        self.means = means
        self.cov_matrix = cov_matrix
        self.size = size
        self.img_sz = img_size
        self.items = self.init_items()

    def is_synthetic(self, ) -> bool:
        return True
    
    def init_items(self, ) -> List:
        res = (
            self.gen_uncorrelated_residues()
            if self.cov_matrix is None else
            self.gen_correlated_residues()
        )
        gts = self.gen_bboxes()
        files = [f"{i:06d}.nan" for i in range(self.size)]
        preds = np.expand_dims(gts + res, 1)
        gts = [[{'bbox': b}] for b in gts]
        items = list(zip(preds, gts, files))
        return items
        
    def gen_uncorrelated_residues(self, max: List = [2.8, 2.5, 8, 2.5],
                                  min: List = [0] * 4) -> np.ndarray:
        n = self.size
        residues = np.random.uniform(min, max, (n, len(self.means)))
        return residues
        
    def gen_correlated_residues(self,) -> np.ndarray:
        n = self.size
        res = np.random.multivariate_normal(self.means, self.cov_matrix, n)
        return res
    
    def gen_bboxes(self, ) -> np.ndarray:
        # Is this (really) necessary? -> ???
        n = self.size
        x = np.random.randint(0, self.img_sz[0], n)
        y = np.random.randint(0, self.img_sz[1], n)
        w = np.random.randint(self.img_sz[0] * 0.02, self.img_sz[0] * 0.2, n)
        h = np.random.randint(self.img_sz[1] * 0.02, self.img_sz[1] * 0.2, n)
        rnd_bboxes = np.stack([x - w/2, y - w/2 , x + w/2, y + w/2], axis=1)
        return rnd_bboxes
    
    def __getitem__(self, index) -> np.ndarray:
        index_is_valid = (
            'int' in str(type(index)) and
            abs(index) < self.size and
            np.isfinite(index)
        )
        if index_is_valid:
            return self.items[index]
        else:
            raise IndexError()
    
    def __len__(self, ) -> int:
        return self.size


class KittiPreprocessor():
    DTS = "KITTI"
    DTS_FIELDS = ["type", "truncated", "occluded", "alpha",
                    "bbox2_left", "bbox2_top", "bbox2_right",
                    "bbox2_bottom", "bbox3_height",
                    "bbox3_width", "bbox3_length", "bbox3_x",
                    "bbox3_y", "bbox3_z", "bbox3_yaw", "score"]
    DTS_CLS_NUMS = {'Car': 0, 'Pedestrian': 1, 'Van': 2,
                      'Cyclist': 3, 'Truck': 4, 'Misc': 5,
                      'Tram': 6, 'Person_sitting': 7, 'DontCare': 8}
    SRC = "datasets/FilteredKitti/raw/training/image_2"
    DEST = "datasets/FilteredKitti/ultralytics"
    
    def __init__(self, data_fields: List = None,
                 class_names: dict = None, **kwargs) -> None:
        self.fields = (data_fields if data_fields is not None
                       else self.DTS_FIELDS)
        self.clsz = (class_names if class_names is not None
                     else self.DTS_CLS_NUMS)

    def prepare_data(self, split: Split, detector: str, **kwargs) -> Any:
        print(f">> Preparing {self.DTS}'s files ...")
        if detector == "YOLOv8 D." and split.dataset.name == self.DTS:
            if not Path(self.DEST).exists():
                Path(self.DEST, "labels").mkdir(parents=True)
                shutil.copytree(self.SRC, self.DEST + "/images")
                self.convert_to_yolo_format(self.SRC, self.DEST)
            data = self.write_yolos_yaml(self.DEST, split, **kwargs)
        return data

    def convert_to_yolo_format(self, src: str, dest: str) -> None:
        """
        Source: https://www.kaggle.com/code/shreydan/kitti-data-yolo-conversion
        """
        print(f"Converting {self.DTS} labels to YOLO format...")
        src = src.replace("image_2", "label_2")
        labels = sorted(list(Path(src).glob('*.txt')))
        for lbl_p in tqdm(labels):
            img_p = str(lbl_p).replace("label_2", "image_2")
            img_p = img_p.replace(".txt", ".png")
            yolo_labels = self.parse_kitti_sample(lbl_p, img_p)
            with open(dest + "/labels/" + lbl_p.name, "w+") as lbl_f:
                [lbl_f.write("{} {} {} {} {}\n".format(*lbl))
                 for lbl in yolo_labels]

    def parse_kitti_sample(self, lbl: str, img: str) -> List:
        with open(lbl) as f:
            reader = csv.DictReader(f, fieldnames=self.fields, delimiter=" ")
            yolo_labels = []
            for row in reader:
                class_num = self.clsz[row["type"]]
                if class_num is not None:
                    size = Image.open(img).size
                    bbox = (
                        float(row["bbox2_left"]),
                        float(row["bbox2_right"]),
                        float(row["bbox2_top"]),
                        float(row["bbox2_bottom"])
                    )
                    yolo_bbox = self.kitti_to_yolo(bbox, size)
                    yolo_label = (class_num,) + yolo_bbox
                    yolo_labels.append(yolo_label)
        return yolo_labels

    def kitti_to_yolo(self, bbox: Tuple, size: Tuple) -> Tuple:
        """
        Source: https://pjreddie.com/media/files/voc_label.py
        """
        dw = 1. / size[0]
        dh = 1. / size[1]
        x = (bbox[0] + bbox[1]) / 2.0
        y = (bbox[2] + bbox[3]) / 2.0
        w = bbox[1] - bbox[0]
        h = bbox[3] - bbox[2]
        x = x * dw
        w = w * dw
        y = y * dh
        h = h * dh
        return (x, y, w, h)

    def write_yolos_yaml(self, dest: str, split: Split,
                         val_size: int = 100, **kwargs) -> str:
        """
        Sources:                
            - https://www.kaggle.com/code/shreydan/kitti-object-detection-yolov8n
            - https://www.kaggle.com/code/gauravkoradiya/yolov8-kitti-object-detection/notebook
        """
        imgs = [f"{dest}/images/{Path(item[0].filename).name}"
                for item in split.tra_subset]
        val_imgs, tra_imgs = imgs[:val_size], imgs[val_size:]
        k, classes = split.label, list(self.clsz.keys())
        config_file = f"{dest}/fold_{k}.yaml"
        config = 'names:\n'
        config += '\n'.join(f'- {c}' for c in classes)
        config += f'\nnc: {len(classes)}'
        config += f'\ntrain: {Path(dest).resolve()}/fold_{k}_train.txt'
        config += f'\nval: {Path(dest).resolve()}/fold_{k}_val.txt'
        with open(f"{dest}/fold_{k}_train.txt", "w") as txt1:
            txt1.write("".join(f'{img}\n' for img in tra_imgs))
        with open(f"{dest}/fold_{k}_val.txt", "w") as txt2:
            txt2.write("".join(f'{img}\n' for img in val_imgs))
        with open(config_file, "w") as yaml:
            yaml.write(config)
        return config_file


class FilteredKitti(Kitti):
    def __init__(self, root: str, transform: Callable[..., Any] = None,
                 target_transform: Callable[..., Any] = None,
                 transforms: Callable[..., Any] = None,
                 cls_names: List = ['Car'], download: bool = False,
                 name: str = "KITTI", **kwargs) -> None:
        super().__init__(root, True, transform,
                         target_transform, transforms, download)
        self.cls_names = cls_names
        self.name = name
        self.preprocessor = KittiPreprocessor(**kwargs)

    def is_synthetic(self, ) -> bool:
        return False

    def __getitem__(self, index: int) -> Tuple[Any, Any]:
        item = super().__getitem__(index)
        img_path = self.get_item_fname(index)
        cls_filtered_objs = [obj for obj in item[1]
                             if obj['type'] in self.cls_names]
        cls_filtered_item = (item[0], cls_filtered_objs, img_path)
        return cls_filtered_item

    def get_item_fname(self, index: int) -> str:
        item = super().__getitem__(index)
        item_path = Path(item[0].filename).name
        return item_path


class BDD100KDownloader():
    """
        This class downloads BDD100K and extracts it into a Kitti-like tree
    """
    N_TRAIN_VAL_IMGS : int = 79853 #69863 #79863 #80000
    label_zip_url: str = "https://dl.cv.ethz.ch/bdd100k/data/bdd100k_det_20_labels_trainval.zip"
    train_zip_url: str = "https://dl.cv.ethz.ch/bdd100k/data/100k_images_train.zip"
    train_md5_url: str = "https://dl.cv.ethz.ch/bdd100k/data/100k_images_train.zip.md5"
    val_zip_url: str = "https://dl.cv.ethz.ch/bdd100k/data/100k_images_val.zip"
    val_md5_url: str = "https://dl.cv.ethz.ch/bdd100k/data/100k_images_val.zip.md5"

    def __init__(self, BDD100K_root: str="datasets/FilteredBDD100K") -> None:
        Path(BDD100K_root).mkdir(exist_ok=True)
        self.BDD100K_root = BDD100K_root

    def __call__(self,) -> None:
        if self.is_extracted():
            return None
        self.download_archives()
        self.check_archives()
        self.unzip_archives()
        self.format_imgs()
        self.format_gts()
        self.organize_folders()

    def is_extracted(self, ) -> None:
        imgs_dir = Path(self.BDD100K_root, 'raw/training/image_2')
        gts_dir = Path(self.BDD100K_root, 'raw/training/label_2')
        src_dirs_exist = imgs_dir.is_dir() and gts_dir.is_dir()
        
        all_img_files = set(imgs_dir.glob("*.png"))
        all_gt_files = set(gts_dir.glob("*.txt"))
        all_files_extracted = (
            len(all_gt_files) == len(all_img_files) == self.N_TRAIN_VAL_IMGS
        )
        #print(len(all_gt_files), len(all_img_files), self.N_TRAIN_VAL_IMGS)
        return src_dirs_exist and all_files_extracted

    def download_archives(self, chunk_sz: int=128) -> None:
        urls = [self.train_zip_url, self.train_md5_url, 
                self.val_zip_url, self.val_md5_url, self.label_zip_url]
        to_be_downloaded = [url for url in urls
                            if not (Path(self.BDD100K_root, 
                                        url.split("/")[-1]).is_file()
                                    or Path(self.BDD100K_root, 'raw',
                                            url.split("/")[-1]).is_file())]
        if to_be_downloaded == []:
            return
        print(">> Downloading archives ...")

        for url in to_be_downloaded:
            save_path = Path(self.BDD100K_root, url.split("/")[-1])
            r = requests.get(url, stream=True)
            with open(save_path, 'wb') as fd:
                for chunk in r.iter_content(chunk_size=chunk_sz):
                    fd.write(chunk)

    def check_archives(self, ) -> None:
        if Path(self.BDD100K_root, 'temp').is_dir():
            return
        
        print(">> MD5-checking the retrieved archives ...")
        for archive in [Path(self.BDD100K_root, "100k_images_train.zip"), 
                        Path(self.BDD100K_root, "100k_images_val.zip")]:
            with open(f"{archive}.md5", 'r') as f:
                expected_md5 = f.readline().split(" ")[0]

            hasher = hashlib.md5()
            with open(archive, 'rb') as f:
                for chunk in iter(lambda: f.read(4096), b''):
                    hasher.update(chunk)
            failed_check = hasher.hexdigest() != expected_md5

            if failed_check:
                print(archive, " did not pass the md5 check!")

    def unzip_archives(self, ) -> None:
        if Path(self.BDD100K_root, 'temp').is_dir():
            return
        
        print(">> Extracting archives ...")
        final_dir = Path(self.BDD100K_root, 'raw')
        final_dir.mkdir(exist_ok=True)
        for archive in [Path(self.BDD100K_root, "100k_images_train.zip"),
                        Path(self.BDD100K_root, "100k_images_val.zip"),
                        Path(self.BDD100K_root, 
                             "bdd100k_det_20_labels_trainval.zip")]:
            with ZipFile(archive, 'r') as zf:
                zf.extractall(Path(self.BDD100K_root, "temp"))
            archive_md5 = Path(f'{archive}.md5')
            archive.rename(Path(final_dir, archive.name))
            if 'labels' not in archive.name:
                archive_md5.rename(Path(final_dir, archive_md5.name))

    def format_imgs(self, ) -> None:
        """
            This function transforms BDD100K .jpg(s) into .png(s)
        """
        d = Path(self.BDD100K_root, 'temp/bdd100k/images/100k')
        if Path(d, 'train_pngs').is_dir() and Path(d, 'val_pngs').is_dir():
            return
        
        print(">> Formating images ...")
        extr_p = Path(self.BDD100K_root, "temp/bdd100k/images/100k/")
        for mode in ["train", "val"]:
            Path(extr_p, f"{mode}_pngs").mkdir(exist_ok=True)
            jpgs = sorted(Path(extr_p, mode).glob("*.jpg"))
            for jpg in tqdm(jpgs):
                png = Image.open(jpg)
                fname = jpg.name.replace('.jpg', '.png')
                png.save(Path(extr_p, f"{mode}_pngs", fname))

    def format_gts(self, ) -> None:
        """
            This function transforms BDD100K labels into KITTI-like labels
        """
        d = Path(self.BDD100K_root, 'temp/bdd100k/images/100k')
        if Path(d,'train_labels').is_dir() and Path(d,'val_labels').is_dir():
            return
        
        print(">> Formating labels ...")
        extr_p = Path(self.BDD100K_root, "temp/bdd100k/images/100k/")
        for mode in ["train", "val"]:
            Path(extr_p, f"{mode}_labels").mkdir(exist_ok=True)
            fp = Path(extr_p.parents[1], f"labels/det_20/det_{mode}.json")
            with open(fp) as f:
                labels = json.load(f)

            for img_lbls in tqdm(labels):
                fname = img_lbls['name'].replace(".jpg", ".txt")
                if 'labels' not in img_lbls:
                    continue

                with open(Path(extr_p, f"{mode}_labels", fname), 'w') as f:
                    formated = []

                    for obj in img_lbls['labels']:
                        gt_fields = self.get_gt_fields(obj)
                        formated.append(" ".join(map(str, gt_fields)) + "\n")
                    f.writelines(formated)
        self.drop_unlabelleds()

    def get_gt_fields(self, obj: dict) -> List:
        gt_fields = [
            obj['category'].replace(' ', '-'), 
            float(obj['attributes']['truncated']),
            int(obj['attributes']['occluded']),
            float('nan'),   # No alpha(s) in BDD,
            * obj['box2d'].values(),
            * ([float('nan')] * 3), # No 3D size
            * ([float('nan')] * 3), # No 3D location
            float('nan') # No rotation_y in BDD100K
        ]
        return gt_fields
    
    def drop_unlabelleds(self, ) -> None:
        imgs = Path(self.BDD100K_root, 
                    'temp/bdd100k/images/100k/', 'train_pngs')
        imgs = set([Path(img).stem for img in imgs.glob("*.png")])
        gts = Path(self.BDD100K_root,
                   'temp/bdd100k/images/100k/', 'train_labels')
        gts = set([Path(gt).stem for gt in gts.glob("*.txt")])
        missing_labels = imgs - gts

        for img in missing_labels:
            Path(self.BDD100K_root, 'temp/bdd100k/images/100k/',
                 'train_pngs', f'{img}.png').unlink()
        self.n_missing = len(missing_labels)

    def organize_folders(self, ) -> None:
        print(">> Organizing folders ...")
        img_dest = Path(self.BDD100K_root, 'raw/training/image_2')
        gt_dest = Path(self.BDD100K_root, 'raw/training/label_2')
        img_dest.mkdir(parents=True, exist_ok=True)
        gt_dest.mkdir(parents=True, exist_ok=True)

        extr_p = Path(self.BDD100K_root, "temp/bdd100k/images/100k/")
        imgs = set(Path(extr_p, 'train_pngs').glob("*.png"))
        imgs |= set(Path(extr_p, 'val_pngs').glob("*.png"))
        gts = set(Path(extr_p, 'train_labels').glob("*.txt"))
        gts |= set(Path(extr_p, 'val_labels').glob("*.txt"))

        pairs = list(zip(sorted(imgs), sorted(gts)))
        assert len(pairs) == len(gts) == len(imgs), \
            (len(pairs), len(gts), len(imgs))
        for i, (img, gt) in enumerate(tqdm(pairs)):
            img.rename(Path(img_dest, f'{i:06d}.png'))
            gt.rename(Path(gt_dest, f'{i:06d}.txt'))

        shutil.rmtree(Path(self.BDD100K_root, "temp"))
        assert self.is_extracted()


class BDD100KPreprocessor(KittiPreprocessor):
    DTS = "BDD100K"
    DTS_FIELDS = ["type", "truncated", "occluded", "alpha",
                  "bbox2_left", "bbox2_top", "bbox2_right",
                  "bbox2_bottom", "bbox3_height",
                  "bbox3_width", "bbox3_length", "bbox3_x",
                  "bbox3_y", "bbox3_z", "bbox3_yaw", "score"]
    DTS_CLS_NUMS = {'pedestrian': 0, 'rider': 1, 'car': 2,
                    'truck': 3, 'bus': 4, 'train': 5, 'motorcycle': 6,
                    'bicycle': 7, 'traffic-light': 8, 'traffic-sign': 9,
                    'other-person': 10, 'trailer': 10, 'other-vehicle': 10}
    # Don't care -> class index = 10
    SRC = "datasets/FilteredBDD100K/raw/training/image_2"
    DEST = "datasets/FilteredBDD100K/ultralytics"


class FilteredBDD100K(FilteredKitti):
    def __init__(self, root: str, transform: Callable[..., Any] = None,
                 target_transform: Callable[..., Any] = None, 
                 transforms: Callable[..., Any] = None, 
                 cls_names: List = ['car'], download: bool = False, 
                 downloader: BDD100KDownloader = BDD100KDownloader(),
                 name: str = "BDD100K", **kwargs) -> None:
        if download:
            downloader()
        super().__init__(root, transform, target_transform, transforms, 
                         cls_names, False, name, **kwargs)
        self.preprocessor = BDD100KPreprocessor()
        
    def download(self) -> None:
        self.downloader()