import torch
import numpy as np
from pathlib import Path
from ultralytics import YOLO
from typing import Tuple, List, Any

from src.data import Split
from src.utils import SEED


class Detector():
    name: str = None
    
    def train(self, split: Split, **kwargs) -> Tuple:
        if self.is_trainable():
            epochs = kwargs.get("epochs", 50)
            cache = kwargs.get("cache", 'ram')
            imgsz = kwargs.get("imgsz", 640)
            device = kwargs.get("device", torch.device('cuda:0'))
            model, dest, data = self.prepare_for_training(split, **kwargs)
            results = model.train(
                data=data, epochs=epochs, cache=cache,
                imgsz=imgsz, project=dest, device=device, seed=SEED
            )
            self.set_model(model)
            results =  dest
        else:
            results = None
        return results

    def is_trainable(self,) -> bool:
        return False

    def set_model(self, model: Any) -> None:
        pass

    def prepare_for_training(self, split: Split, **kwargs) -> Tuple:
        detector, dest = self.prepare_model(split, **kwargs)
        data = split.dataset.preprocessor.prepare_data(
            split, self.name, **kwargs)
        return detector, dest, data

    def prepare_model(self, split: Split, from_scratch: bool = False,
                      **kwargs) -> Tuple:
        pass


class YOLOv8(Detector):
    def __init__(self, name: str = "YOLOv8 D.", 
                 weights: str = "models/YOLOv8/initial_yolov8n.pt" ) -> None:
        self.name = name
        self.yolov8_nn = YOLO(weights)

    def __call__(self, item: Tuple) -> np.ndarray:
        preds = self.yolov8_nn.predict(item[0], verbose=False)[0]
        return preds

    def is_trainable(self) -> bool:
        return True

    def set_model(self, model: Any) -> None:
        self.yolov8_nn = model
        Path('yolov8n.pt').unlink(missing_ok=True)

    def prepare_model(self, split: Split, from_scratch: bool = False,
                      **kwargs) -> Tuple:
        model = YOLO('yolov8n.yaml')
        dest = f'models/YOLOv8/fold_{split.label}'
        Path(dest).mkdir(parents=True, exist_ok=True)
        if Path('yolov8n.pt').is_file():
            Path('yolov8n.pt').replace('models/YOLOv8/yolov8n.pt')
        if not from_scratch:
            model = YOLO('models/YOLOv8/initial_yolov8n.pt')
        return model, dest
