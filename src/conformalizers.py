import numpy as np
from abc import ABC, abstractmethod
from typing import Callable, List, Any
from copulae import GumbelCopula
from copulae.core import pseudo_obs
from pandas import DataFrame, MultiIndex
from scipy.optimize import (
    differential_evolution,
    NonlinearConstraint,
    Bounds
)
from src.metrics import abs_diff
from src.utils import SEED


class Conformalizer(ABC):
    def __init__(self, dissim_fx: Callable = abs_diff,
                 name: str = "") -> None:
        self.get_dissimilarity = dissim_fx
        self.significance_th = None
        self.name = name
        self.dissim_th_index = MultiIndex.from_product(
            (["x_min", "y_min", "x_max", "y_max"], ["Exact value"]),
            names=["object's fields", "Bounds"])

    def set_parameters(self, **kwargs) -> None:
        self.epsilon = kwargs['epsilon']

    def calibrate(self, pairs: DataFrame) -> DataFrame:
        print(f">> Calibrating {self.name} ...")
        self.compute_dissimilarities(pairs)
        self.compute_threshold(pairs)
        cal_results = self.conformalize(pairs)
        return cal_results

    def compute_dissimilarities(self, pairs: DataFrame) -> None:
        dissims = self.get_dissimilarity(pairs)
        self.dissimilarities = dissims

    def compute_threshold(self, pairs: DataFrame) -> None:
        n = (1 + 1/len(self.dissimilarities))
        q = min(n * (1 - self.get_dims_epsilons(self.epsilon, 4)), 0.9999)
        q = max(q, 0.0001)
        self.dissim_th = self.dissimilarities.quantile(q)

    def conformalize(self, pairs: DataFrame,
                     obj_fields: List = ["x_min", "y_min", "x_max", "y_max"],
                     tags: List = ["UQ methods", "object's fields", "Bounds"]
                     ) -> DataFrame:
        print(f">> Inferring intervals with {self.name} ...")
        margins = self.dissim_th * [-1, -1, 1, 1]
        lower_bounds = pairs["Predictions"] - margins
        upper_bounds = pairs["Predictions"] + margins
        lower_bounds = DataFrame(
            data=lower_bounds.values,
            index=pairs.index,
            columns=MultiIndex.from_product(
                ([self.name], obj_fields, ["Lower bound"]),
                names=tags
            )
        )
        upper_bounds = DataFrame(
            data=upper_bounds.values,
            index=pairs.index,
            columns=MultiIndex.from_product(
                ([self.name], obj_fields, ["Upper bound"]),
                names=tags
            )
        )
        conf_bounds = pairs.copy().join(lower_bounds).join(upper_bounds)
        return conf_bounds

    def get_margins(self, pairs: DataFrame = None, img_idx: int = None,
                    cls_idx: int = None, obj_idx: int = None) -> np.ndarray:
        margins = self.dissim_th * [-1, -1, 1, 1]
        return margins


class BonferroniAdditiveConformalizer(Conformalizer):
    def __init__(self, dissim_fx: Callable[..., Any] = abs_diff,
                 name: str = "Bonferroni") -> None:
        super().__init__(dissim_fx, name)

    def get_dims_epsilons(self, gbl_epsilon: float, n_dims: int = 4) -> float:
        dims_eps = gbl_epsilon / n_dims
        return dims_eps


class MaxAdditiveConformalizer(Conformalizer):
    def __init__(self, dissim_fx: Callable[..., Any] = abs_diff,
                 name: str = "Max additive") -> None:
        super().__init__(dissim_fx, name)

    def compute_threshold(self, pairs: DataFrame) -> None:
        p = (1 - self.epsilon) * (1 + 1/len(self.dissimilarities))
        p = max(min(p, 0.9999), 0.0001)     # Why not 1. & 0. ?
        q = self.dissimilarities.max(axis=1).quantile(p)
        self.dissim_th = DataFrame(data=[q]*4, index=self.dissim_th_index)[0]


class ImprovedConformalizer(Conformalizer, ABC):
    def compute_threshold(self, pairs: DataFrame) -> None:
        # res.x stands for epsilon_t
        zeros, epsilons = [0.000001]*4, [self.epsilon]*4
        x_bounds = Bounds(lb=zeros, ub=epsilons, keep_feasible=True)
        self.iteration = 0
        cc = self.get_copula_constraint()
        res = differential_evolution(
            self.get_inefficiency_loss, workers=-1,
            bounds=x_bounds, callback=self.optim_callback,
            constraints=(cc,), popsize=20, maxiter=200, seed=SEED
        )
        print(f">> Final results: \n\t eps_g:{self.epsilon:.2f}, ",
              f"loss:{res.fun}, eps_t(s):{res.x}, success:{res.success}"
              )
        self.update_dissim_th(res.x)

    def optim_callback(self, x: Any, convergence: Any) -> None:
        if self.iteration == 0:
            print(f">> Optimizing efficiency for '{self.__class__.__name__}':")
        msg = f"\tIteration n°{self.iteration}, eps_g:{self.epsilon}"
        msg += f"eps: {x}, convergence: {convergence}"
        print(msg, end="\r" if convergence < 1 else "\n")
        self.iteration += 1

    @abstractmethod
    def get_copula_constraint(self,) -> Any:
        pass

    def get_inefficiency_loss(self, x: Any) -> float:
        self.update_dissim_th(x)
        loss_val = self.dissim_th.product()  # i.e., volume / (2 ** 4)
        return loss_val

    def update_dissim_th(self, x: List) -> None:
        q = (1 - x) * (1 + 1/len(self.dissimilarities))
        q[q >= 1.0] = 0.9999
        q[q <= 0.0] = 0.0001
        self.dissim_th = DataFrame(
            data=np.diag(self.dissimilarities.quantile(q)),
            index=self.dissim_th_index)[0]


class ImprovedIndependentConformalizer(ImprovedConformalizer):
    def __init__(self, dissim_fx: Callable[..., Any] = abs_diff,
                 name: str = "Independent DE-CCP") -> None:
        super().__init__(dissim_fx, name)

    def get_copula_constraint(self) -> Any:
        def copulas_loss_(x):
            if any(x <= 0) or any( x > self.epsilon):
                # When DE evaluates out-of-bounds values despite x_bounds!
                return  -1
            return np.prod(1 - x) - (1 - self.epsilon)
        independent_con = NonlinearConstraint(copulas_loss_, 0, 1, 
                                              keep_feasible=True)
        return independent_con


class ImprovedGumbelConformalizer(ImprovedConformalizer):
    def __init__(self, dissim_fx: Callable[..., Any] = abs_diff,
                 name: str = "Gumbel DE-CCP") -> None:
        super().__init__(dissim_fx, name)

    def get_copula_constraint(self) -> Any:
        self.cop = GumbelCopula(dim=4)
        self.cop.fit(self.dissimilarities.values)
        independent_con = NonlinearConstraint(self.copulas_loss_, 0, 1, 
                                              keep_feasible=True)
        return independent_con

    def copulas_loss_(self, x: Any) -> float:
        if any(x <= 0) or any( x > self.epsilon):
            return  -1
        value = self.cop.cdf(1 - x) - (1 - self.epsilon)
        return value


class ImprovedEmpiricalConformalizer(ImprovedConformalizer):
    def __init__(self, dissim_fx: Callable[..., Any] = abs_diff,
                 name: str = "Empirical DE-CCP") -> None:
        super().__init__(dissim_fx, name)

    def get_copula_constraint(self,) -> Any:
        self.pseudos = pseudo_obs(self.dissimilarities.values)
        empirical_con = NonlinearConstraint(self.copulas_loss_, 0, 1,
                                            keep_feasible=True)
        return empirical_con

    def copulas_loss_(self, x: Any) -> float:
        if any(x <= 0) or any( x > self.epsilon):
            return  -1
        # According to Soundouss et al. (2021), eq(27)
        assert self.pseudos is not None
        ineqs = np.less_equal(self.pseudos, 1 - x)
        value = (np.mean(np.all(ineqs, axis=1)) - (1 - self.epsilon))
        return value
